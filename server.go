package main

import "github.com/codegangsta/martini"

func main() {
  m := martini.Classic()
  m.Use(martini.Static("frontend/dist")) 
/*  m.Get("/", func() string {
    return "Hello world!"
  })*/
  m.Run()
}
